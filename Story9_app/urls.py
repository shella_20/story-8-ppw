from django.urls import path, include
from . import views

app_name = 'Story9_app'

urlpatterns = [
    path('', views.index, name = 'Story9_home'),
]
