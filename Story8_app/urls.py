from django.urls import path, include
from . import views

app_name = 'Story8_app'

urlpatterns = [
    path('', views.index),
]
